var express = require('express');
var router = express.Router();
var Bookmark=require('../models/bookmark');

/* GET home page. */
router.get('/', function(req, res) {
  res.sendfile('./public/index.html' ); //load a single page
});

router.get('/bookmarks',function(req,res){
	//use mongoose to get all bookmarks in the database
	Bookmark.find(function(err,bookmarks){
		//if there is an error retrieving send the error
		if (err)
			res.send(err)
		  res.render('bookmarks', { bookmarks: bookmarks,title:'Good' });

	});

});


//api
//get all bookmarks
router.get('/api/bookmarks',function(req,res){
	//use mongoose to get all bookmarks in the database
	Bookmark.find(function(err,bookmarks){
		//if there is an error retrieving send the error
		if (err)
			res.send(err)
		res.json(bookmarks);
	});

});

router.post('/api/bookmarks',function(req,res){
	//create a bookmark,information comes from AJAX request from Angular
	Bookmark.create({
		title:req.body.title,
		webpoint:req.body.webpoint,
		description:req.body.description,
		done:false
	},function(err,bookmark){
		if (err)
			res.send(err);

		//get and return all the bookmarks after you create another
		Bookmark.find(function(err,bookmarks){
			if (err)
				res.send(err)
			res.json(bookmarks);
		});
	});
});

//delete a bookmark
router.delete('/api/bookmarks/:bookmark_id',function(req,res){
	Bookmark.remove({
		_id:req.params.bookmark_id
	},function(err,bookmark){
		if (err)
			res.send(err);
		//get and return all the bookmarks after you alter another
		Bookmark.find(function(err,bookmarks){
			if (err)
				res.send(err)
			res.json(bookmarks);

		}); 
	});
});
module.exports = router;
