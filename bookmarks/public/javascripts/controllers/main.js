angular.module('bookmarkController', [])

	// inject the Bookmark service factory into our controller
	.controller('mainController', ['$scope','$http','Bookmarks', function($scope, $http, Bookmarks) {
		$scope.formData = {};
		$scope.loading = true;
		$scope.info=false;
		$scope.info_text="";

		// GET =====================================================================
		// when landing on the page, get all todos and show them
		// use the service to get all the todos
		Bookmarks.get()
			.success(function(data) {
				$scope.bookmarks = data;
				$scope.loading = false;
			});

		// CREATE ==================================================================
		// when submitting the add form, send the text to the node API
		$scope.createBookmark = function() {
			$scope.loading = true;

			// validate the formData to make sure that something is there
			// if form is empty, nothing will happen
			if ($scope.formData.title != undefined ) {

				// call the create function from our service (returns a promise object)
				Bookmarks.create($scope.formData)

					// if successful creation, call our get function to get all the new todos
					.success(function(data) {
						$scope.loading = false;
						$scope.formData = {}; // clear the form so our user is ready to enter another
						$scope.bookmarks = data; // assign our new list of todos
					})
					.error(function(data){
						$scope.loading=false;
						$scope.formData={'title':'error'};
						$scope.bookmarks=data;
					});
					}
					else{
						$scope.formData={};
						$scope.loading=false;
						$scope.info=true;
						$scope.info_text='error';
					}
			
		};

		// DELETE ==================================================================
		// delete a bookmark after checking it
		$scope.deleteBookmark = function(id) {
			$scope.loading = true;

			Bookmarks.delete(id)
				// if successful creation, call our get function to get all the new todos
				.success(function(data) {
					$scope.loading = false;
					$scope.bookmarks = data; // assign our new list of todos
				});
		};
	}]);