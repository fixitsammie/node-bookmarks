var mongoose = require('mongoose');

module.exports = mongoose.model('Bookmark', {
	title : {type : String, default: ''},
	webpoint:{type : String, default: ''},
	description:{type : String, default: ''}
});